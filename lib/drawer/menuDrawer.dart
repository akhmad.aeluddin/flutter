import 'package:ale/drawer/form/foto.dart';
import 'package:flutter/material.dart';

// ignore: camel_case_types
class MenuDrawer extends StatelessWidget {
  final padding = EdgeInsets.symmetric(horizontal: 20);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Material(
        color: Colors.blue,
        child: ListView(
          children: <Widget>[
            const SizedBox(
              height: 200,
              child: Center(
                child: Text('data'),
              ),
            ),
            ListTile(
              hoverColor: Colors.black,
              leading: Icon(
                Icons.home,
                color: Colors.white,
              ),
              title: Text(
                'Home',
                style: TextStyle(color: Colors.white),
              ),
              onTap: () => selectdItem(context, 1),
            ),
            ListTile(
              hoverColor: Colors.black,
              leading: Icon(
                Icons.image,
                color: Colors.white,
              ),
              title: Text(
                'Images',
                style: TextStyle(color: Colors.white),
              ),
              onTap: () => selectdItem(context, 0),
            ),
            ListTile(
              hoverColor: Colors.black,
              leading: Icon(
                Icons.person,
                color: Colors.white,
              ),
              title: Text(
                'Tentang',
                style: TextStyle(color: Colors.white),
              ),
              onTap: () {},
            ),
          ],
        ),
      ),
    );
  }

  void selectdItem(BuildContext context, int index) {
    switch (index) {
      case 0:
        Navigator.of(context).push(MaterialPageRoute(
          builder: (context) => Foto(),
        ));
        break;
      case 1:
        Navigator.of(context).pop();
        break;
    }
  }
}
