import 'package:ale/btmbar/home.dart';
import 'package:ale/btmbar/officialSore.dart';
import 'package:ale/btmbar/transaksi.dart';
import 'package:ale/btmbar/wishList.dart';
import 'package:ale/drawer/menuDrawer.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Mn(),
    );
  }
}

class Mn extends StatefulWidget {
  @override
  _MnState createState() => _MnState();
}

class _MnState extends State<Mn> {
  int _currentIndex = 0;
  final List<Widget> title = [
    Text('Home'),
    Text('Official Store'),
    Text('WishList'),
    Text('Transaksi')
  ];
  final List<Widget> tabs = [
    Home(),
    OficialStore(),
    WishList(),
    Transaksi(),
  ];

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(
          title: title[_currentIndex],
        ),
        body: tabs[_currentIndex],
        bottomNavigationBar: BottomNavigationBar(
          currentIndex: _currentIndex,
          unselectedItemColor: Colors.white,
          selectedItemColor: Colors.red,
          items: [
            BottomNavigationBarItem(
              icon: Icon(Icons.home),
              backgroundColor: Colors.blue,
              label: 'Home',
            ),
            BottomNavigationBarItem(
              backgroundColor: Colors.blue,
              icon: Icon(Icons.store),
              label: 'OficialStore',
            ),
            BottomNavigationBarItem(
              backgroundColor: Colors.blue,
              icon: Icon(Icons.favorite),
              label: 'WishList',
            ),
            BottomNavigationBarItem(
              backgroundColor: Colors.blue,
              icon: Icon(Icons.note_add),
              label: 'Transaksi',
            ),
          ],
          onTap: (index) {
            setState(() {
              _currentIndex = index;
            });
          },
        ),
        endDrawer: MenuDrawer(),
      );
}
